import React, { Component } from 'react';
import PropTypes from 'prop-types';

const propTypes = {
    name: PropTypes.string.isRequired,
    onClick: PropTypes.func,
    title: PropTypes.string.isRequired
}

class Register extends Component {

    render() {
        const { title, name, onClick } = this.props
        return (
            <div className="Register">
                <h2>Title: {title}</h2>
                <h2>Name: {name}</h2>
                <button onClick={onClick}>onClick</button>
            </div>
        )
    }
}
// proptypes are for validation purposes that is useful
Register.propTypes = propTypes;
export default Register;
