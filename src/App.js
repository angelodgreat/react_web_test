import React, { Component } from 'react';
import './App.css';
import Register from './Components/Register'

class App extends Component {

  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this)
    this.onClick = this.onClick.bind(this)
    this.state = {
      title: 'React Tutorial'
    }
  }

  onSubmit(event) {
    event.preventDefault();
    alert(this.input.value)
  }

  onClick() {
    // alert('From Register Component')
    this.setState({
      title: 'New React'
    })

  }

  render() {
    const list = [
      'Item 1',
      'Item 2'
    ]

    return (
      <div className="App">
        <h1>{this.state.title}</h1>
        {/* {
          list.map(item => {
            return (
              // <li>{item}</li>
              // <div onClick={this.onClick}>{item}</div>
              <div key={item} onMouseEnter={this.onMouseEnter}>{item}</div>
            )

          })
        } */}

        <button onClick={this.onClick}>Test</button>

        <Register
          title="Register"
          name="Angelo"
          onClick={this.onClick}
        />

        <form onSubmit={this.onSubmit}>
          <input ref={input => this.input = input} />
        </form>
      </div>
    );
  }
}

export default App;
